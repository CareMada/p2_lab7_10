package ro.ubb.catalog.core.service;

import org.springframework.beans.factory.parsing.Problem;
import ro.ubb.catalog.core.model.Discipline;
import ro.ubb.catalog.core.model.Student;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by radu.
 */

public interface DisciplineService {
    List<Discipline> findAll();

    Discipline createDiscipline(String name, String teacher, Integer credits);

    //Discipline findProblem(Long problemId);

    Discipline updateProblem(Long problemId, String name, String teacher, Integer credits,
                          Set<Long> problems);

    void deleteDiscipline(Long disciplineId);

    Discipline updateProblemGrades(Long problemId, Map<Long, Integer> grades); //dictionary: id problem, grade
}
