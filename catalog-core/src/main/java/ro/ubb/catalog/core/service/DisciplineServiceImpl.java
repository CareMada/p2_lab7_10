package ro.ubb.catalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.model.Discipline;
import ro.ubb.catalog.core.model.Student;
import ro.ubb.catalog.core.repository.DisciplineRepository;
import ro.ubb.catalog.core.repository.StudentRepository;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by radu.
 */

@Service
public class DisciplineServiceImpl implements DisciplineService {

    private static final Logger log = LoggerFactory.getLogger(DisciplineServiceImpl.class);


    @Autowired
    private DisciplineRepository disciplineRepository;
    @Autowired
    private StudentRepository studentRepository;

    @Override
    @Transactional
    public List<Discipline> findAll() {
        log.trace("findAll");

        List<Discipline> disciplines = disciplineRepository.findAll();

        log.trace("findAll: disciplines={}", disciplines);

        return disciplines;
    }

    @Override
    public Discipline createDiscipline(String name, String teacher, Integer credits) {
        log.trace("createDiscipline: name={}, teacher={}, credits={}", name, teacher, credits);

        Discipline d = Discipline.builder()
                .credits(credits)
                .name(name)
                .teacher(teacher)
                .build();
        Discipline discipline = disciplineRepository.save(d);

        log.trace("createDiscipline: discipline={}", discipline);

        return discipline;
    }

//    @Override
//    public Discipline findProblem(Long problemId) {
//        log.trace("findProblem: problemId={}", problemId);
//
//        Discipline discipline = disciplineRepository.findOne(problemId);
//
//        log.trace("findProblem: problem={}", discipline);
//
//        return discipline;
//    }
    @Override
    @Transactional
    public Discipline updateProblem(Long problemId, String name, String teacher, Integer credits, Set<Long> students) {
        log.trace("updateProblem: problemId={}, teacher={}, name={}, credits={}, problems={}",
                problemId, teacher, name, credits, students);

        Discipline discipline = disciplineRepository.findOne(problemId);
        discipline.setTeacher(teacher);
        discipline.setName(name);
        discipline.setCredits(credits);

        discipline.getStudents().stream()
                .map(d -> d.getId())
                .forEach(id -> {
                    if (students.contains(id)) {
                        students.remove(id);
                    }
                });
        List<Student> studentList = studentRepository.findAll(students);
        studentList.stream().forEach(d -> discipline.addStudent(d));

        log.trace("updateProblem: problem={}", discipline);

        return discipline;
    }

    @Override
    public void deleteDiscipline(Long disciplineId) {
        log.trace("deleteDiscipline: disciplineId={}", disciplineId);

        disciplineRepository.delete(disciplineId);

        log.trace("deleteDiscipline - method end");
    }

    @Override
    @Transactional   //interactionare cu db
    public Discipline updateProblemGrades(Long problemId, Map<Long, Integer> grades) {
        log.trace("updateProblemGrades: problemId={}, grades={}", problemId, grades);

        Discipline discipline = disciplineRepository.findOne(problemId);
        discipline.getStudentDisciplines().stream()
                .forEach(sd -> sd.setGrade(grades.get(sd.getDiscipline().getId())));

        log.trace("updateProblemGrades: student={}", discipline);
        return discipline;
    }
}
