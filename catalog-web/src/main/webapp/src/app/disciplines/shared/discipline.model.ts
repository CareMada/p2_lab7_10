export class Discipline {
    id: number;
    name: string;
    teacher: string;
    credits: number;
    students: number[];
}