import {Injectable} from "@angular/core";
import {Headers, Http, Response} from "@angular/http";
import {StudentDiscipline} from "./student-discipline.model";

import {Observable} from "rxjs";

@Injectable()
export class StudentDisciplineService {
    private studentDisciplineUrl = 'http://localhost:8080/api/grades';
    private headers = new Headers({'Content-Type': 'application/json'});

    constructor(private http: Http) {
    }

    getStudentDisciplines(studentID: number): Observable<StudentDiscipline[]> {
        return this.http.get(`${this.studentDisciplineUrl}/${studentID}`, this.headers)
            .map(this.extractData)
            .catch(this.handleError);
    }

    private extractData(res: Response) {
        let body = res.json();
        return body.studentDisciplines || {};
    }

    private handleError(error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    saveGrades(studentId: number, disciplineIdsGrades: Object): Observable<StudentDiscipline[]> {
        const url = `${this.studentDisciplineUrl}/${studentId}`;
        let studentDisciplines = this.createStudentDisciplines(studentId, disciplineIdsGrades);
        console.log("grades: ",studentDisciplines);
        console.log("request: ",JSON.stringify({"studentDisciplines": studentDisciplines}));
        return this.http
            .put(url, JSON.stringify({"studentDisciplines": studentDisciplines}), {headers: this.headers})
            .map(this.extractData)
            .catch(this.handleError);
    }

    private createStudentDisciplines(studentId: number, disciplineIdsGrades: Object): StudentDiscipline[] {
        const arr: StudentDiscipline[] = [];
        Object.keys(disciplineIdsGrades).forEach(disciplineId => {
            const sd = new StudentDiscipline(
                studentId,
                parseInt(disciplineId),
                null,
                disciplineIdsGrades[disciplineId]);
            arr.push(sd);
        });
        return arr;
    }
}