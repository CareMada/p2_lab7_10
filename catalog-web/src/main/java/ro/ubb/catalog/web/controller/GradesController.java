package ro.ubb.catalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.model.Student;
import ro.ubb.catalog.core.model.StudentDiscipline;
import ro.ubb.catalog.core.service.StudentService;
import ro.ubb.catalog.web.converter.StudentDisciplineConverter;
import ro.ubb.catalog.web.dto.StudentDisciplineDto;
import ro.ubb.catalog.web.dto.StudentDisciplinesDto;
import ro.ubb.catalog.web.dto.StudentDto;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by radu.
 */

@RestController
public class GradesController {
    private static final Logger log = LoggerFactory.getLogger(GradesController.class);

    @Autowired
    private StudentService studentService;

    @Autowired
    private StudentDisciplineConverter studentDisciplineConverter;

    @RequestMapping(value = "/grades/{studentId}", method = RequestMethod.GET)
    public StudentDisciplinesDto getStudentDisciplines(
            @PathVariable final Long studentId) {
        log.trace("getStudentDisciplines: studentId={}", studentId);

        Student student = studentService.findStudent(studentId);

        Set<StudentDiscipline> studentDisciplines = student.getStudentDisciplines();
        Set<StudentDisciplineDto> studentDisciplineDtos = studentDisciplineConverter
                .convertModelsToDtos(studentDisciplines);


        StudentDisciplinesDto result = new StudentDisciplinesDto(studentDisciplineDtos);

        log.trace("getStudentDisciplines: result={}", result);

        return result;
    }

    @RequestMapping(value = "/grades/{studentId}", method = RequestMethod.PUT)
    public StudentDisciplinesDto updateStudentGrades(
            @PathVariable final Long studentId,
            @RequestBody final StudentDisciplinesDto studentDisciplinesDto) {
        log.trace("updateStudentGrades: studentId={}, studentDisciplinesDto={}",
                studentId, studentDisciplinesDto);

        Map<Long, Integer> grades = studentDisciplineConverter.convertDtoToMap(studentDisciplinesDto);
        Student student = studentService.updateStudentGrades(studentId, grades);

        Set<StudentDisciplineDto> studentDisciplineDtos = studentDisciplineConverter.
                convertModelsToDtos(student.getStudentDisciplines());
        StudentDisciplinesDto result = new StudentDisciplinesDto(studentDisciplineDtos);

        log.trace("getStudentDisciplines: result={}", result);

        return result;
    }

//    @RequestMapping(value = "/students/{studentId}", method = RequestMethod.PUT)
//    public Map<String, StudentDto> updateStudent(
//            @PathVariable final Long studentId,
//            @RequestBody final Map<String, StudentDto> studentDtoMap) {
//        log.trace("updateStudent: studentId={}, studentDtoMap={}", studentId, studentDtoMap);
//
//        StudentDto studentDto = studentDtoMap.get("student");
//        Student student = studentService.updateStudent(studentId, studentDto.getSerialNumber(),
//                studentDto.getName(), studentDto.getGroupNumber(), studentDto.getDisciplines());
//
//        Map<String, StudentDto> result = new HashMap<>();
//        result.put("student", studentConverter.convertModelToDto(student));
//
//        log.trace("updateStudent: result={}", result);
//
//        return result;
//    }
}
