package ro.ubb.catalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.model.Discipline;
import ro.ubb.catalog.core.model.Student;
import ro.ubb.catalog.core.service.DisciplineService;
import ro.ubb.catalog.web.converter.DisciplineConverter;
import ro.ubb.catalog.web.dto.DisciplineDto;
import ro.ubb.catalog.web.dto.DisciplinesDto;
import ro.ubb.catalog.web.dto.EmptyJsonResponse;
import ro.ubb.catalog.web.dto.StudentDto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by radu.
 */

@RestController
public class DisciplineController {
    private static final Logger log = LoggerFactory.getLogger(DisciplineController.class);

    @Autowired
    private DisciplineService disciplineService;

    @Autowired
    private DisciplineConverter disciplineConverter;


    @RequestMapping(value = "/disciplines", method = RequestMethod.GET)
    public DisciplinesDto getDisciplines() {
        log.trace("getDisciplines");

        List<Discipline> disciplines = disciplineService.findAll();

        log.trace("getDisciplines: disciplines={}", disciplines);

        return new DisciplinesDto(disciplineConverter.convertModelsToDtos(disciplines));
    }

    @RequestMapping(value = "/disciplines", method = RequestMethod.POST)
    public Map<String, DisciplineDto> createDiscipline(
            @RequestBody final Map<String, DisciplineDto> disciplineDtoMap) {
        log.trace("createDiscipline: disciplineDtoMap={}", disciplineDtoMap);

        DisciplineDto disciplineDto = disciplineDtoMap.get("discipline");
        Discipline discipline = disciplineService.createDiscipline(
                disciplineDto.getName(), disciplineDto.getTeacher(), disciplineDto.getCredits());

        Map<String, DisciplineDto> result = new HashMap<>();
        result.put("discipline", disciplineConverter.convertModelToDto(discipline));

        log.trace("updateDiscipline: result={}", result);

        return result;
    }

    @RequestMapping(value = "/disciplines/{disciplineId}", method = RequestMethod.PUT)
    public Map<String, DisciplineDto> updateDiscipline(
            @PathVariable final Long disciplineId,
            @RequestBody final Map<String, DisciplineDto> disciplineDtoMap) {
        log.trace("updateDiscipline: disciplineId={}, disciplineDtoMap={}", disciplineId, disciplineDtoMap);

        DisciplineDto disciplineDto = disciplineDtoMap.get("discipline");
        Discipline discipline = disciplineService.updateProblem(disciplineId, disciplineDto.getName(),
                disciplineDto.getTeacher(), disciplineDto.getCredits(), disciplineDto.getStudents());


        Map<String, DisciplineDto> result = new HashMap<>();
        result.put("discipline", disciplineConverter.convertModelToDto(discipline));

        log.trace("updateDiscipline: result={}", result);

        return result;
    }

    @RequestMapping(value = "disciplines/{disciplineId}", method = RequestMethod.DELETE)
    public ResponseEntity deleteDiscipline(@PathVariable final Long disciplineId) {
        log.trace("deleteDiscipline: disciplineId={}", disciplineId);

        disciplineService.deleteDiscipline(disciplineId);

        log.trace("deleteDiscipline - method end");

        return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
    }

}
