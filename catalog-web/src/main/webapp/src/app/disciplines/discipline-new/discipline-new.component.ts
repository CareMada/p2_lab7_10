import {Component, Input} from "@angular/core";
import {Location} from '@angular/common';
import {Discipline} from "../shared/discipline.model";
import {DisciplineService} from "../shared/discipline.service";



@Component({
    moduleId: module.id,
    selector: 'ubb-discipline-new',
    templateUrl: './discipline-new.component.html',
    styleUrls: ['./discipline-new.component.css'],
})
export class DisciplineNewComponent {
    @Input() discipline: Discipline;

    constructor(private disciplineService: DisciplineService,
                private location: Location) {
    }

    goBack(): void {
        this.location.back();
    }


    save(name, teacher, credits): void {
        if (!this.isValid(name, teacher, credits)) {
            console.log("all fields are required ");
            alert("all fields are required; credits has to be an int");
            return;
        }
        this.disciplineService.create(name, teacher, credits)
            .subscribe(_ => this.goBack());
    }

    private isValid(name, teacher, credits) {
        if (!name || !teacher || !credits) {
            console.log("all fields are required");
            return false;
        }
        if (!Number.isInteger(Number(credits))) {
            console.log("credits has to be an int");
            return false;
        }
        //TODO other validations
        return true;
    }
}