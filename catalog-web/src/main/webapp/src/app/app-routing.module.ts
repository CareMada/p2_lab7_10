import {NgModule}             from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {StudentDetailComponent} from "./students/student-detail/student-detail.component";
import {StudentsComponent} from "./students/students.component";
import {DisciplinesComponent} from "./disciplines/disciplines.component";
import {DisciplineDetailComponent} from "./disciplines/discipline-detail/discipline-detail.component";
import {StudentNewComponent} from "./students/student-new/student-new.component";
import {DisciplineNewComponent} from "./disciplines/discipline-new/discipline-new.component";
import {DisciplineEnrollComponent} from "./discipline-enroll/discipline-enroll.component";
import {GradesComponent} from "./grades/grades.component";


const routes: Routes = [
    // { path: '', redirectTo: '/home', pathMatch: 'full' },
    {path: 'students', component: StudentsComponent},
    {path: 'student/detail/:id', component: StudentDetailComponent},
    {path: 'disciplines', component: DisciplinesComponent},
    {path: 'discipline/detail/:id', component: DisciplineDetailComponent},
    {path: 'student/new', component: StudentNewComponent},
    {path: 'discipline/new', component: DisciplineNewComponent},
    {path: 'enroll', component: DisciplineEnrollComponent},
    {path: 'grades', component: GradesComponent},
];
@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
