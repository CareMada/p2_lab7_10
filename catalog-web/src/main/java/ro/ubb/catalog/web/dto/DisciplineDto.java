package ro.ubb.catalog.web.dto;

import lombok.*;

import java.util.Set;

/**
 * Created by radu.
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class DisciplineDto extends BaseDto {
    private String name;
    private String teacher;
    private Integer credits;
    private Set<Long> students;

    @Override
    public String toString() {
        return "DisciplineDto{" +
                "name='" + name + '\'' +
                ", teacher='" + teacher + '\'' +
                ", credits=" + credits +
                ", students=" + students +
                '}';
    }
}
