import {Injectable} from '@angular/core';
import {Http, Response, Headers} from "@angular/http";

import {Discipline} from "./discipline.model";

import {Observable} from "rxjs";
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';


@Injectable()
export class DisciplineService {
    private disciplinesUrl = 'http://localhost:8080/api/disciplines';
    private headers = new Headers({'Content-Type': 'application/json'});

    constructor(private http: Http) {
    }

    getDisciplines(): Observable<Discipline[]> {
        return this.http.get(this.disciplinesUrl)
            .map(this.extractData)
            .catch(this.handleError);
    }

    private extractData(res: Response) {
        let body = res.json();
        return body.disciplines || {};
    }

    private handleError(error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    getDiscipline(id: number): Observable<Discipline> {
        return this.getDisciplines()
            .map(disciplines => disciplines.find(discipline => discipline.id === id));
    }

    create(name: string, teacher: string, credits: number): Observable<Discipline> {
        let discipline = {name, teacher, credits};
        return this.http
            .post(this.disciplinesUrl, JSON.stringify({"discipline": discipline}), {headers: this.headers})
            .map(this.extractDisciplineData)
            .catch(this.handleError);
    }

    private extractDisciplineData(res: Response) {
        let body = res.json();
        return body.discipline || {};
    }

    delete(id: number): Observable<void> {
        const url = `${this.disciplinesUrl}/${id}`;
        return this.http
            .delete(url, {headers: this.headers})
            .map(() => null)
            .catch(this.handleError);
    }

    update(discipline): Observable<Discipline> {
        const url = `${this.disciplinesUrl}/${discipline.id}`;
        return this.http
            .put(url, JSON.stringify({"discipline": discipline}), {headers: this.headers})
            .map(this.extractDisciplineData)
            .catch(this.handleError);
    }

}
